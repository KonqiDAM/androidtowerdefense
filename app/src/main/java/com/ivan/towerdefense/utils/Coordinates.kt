package com.ivan.towerdefense.utils

class Coordinates(x: Int, y: Int)
{
    public val x = x
    public val y = y
    override fun equals(other: Any?): Boolean {
        return (other as Coordinates).x == x && (other as Coordinates).y == y;
    }

    override fun toString(): String {
        return "Point: $x, $y";
    }
}