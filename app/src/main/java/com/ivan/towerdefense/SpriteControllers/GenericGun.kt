package com.ivan.towerdefense.SpriteControllers

import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import com.ivan.towerdefense.R
import kotlin.math.abs
import kotlin.math.atan2
import kotlin.random.Random


const val SMOOTHPERC: Float = 0.1f
const val PAUSE: Int = 120

open class GenericGun(image: Bitmap, x: Int, y: Int) : GenericItem(image, x, y) {

    var lastShoot: Int = 0
    val RANGE: Double = image.width * 2.6

    private fun findNearest(enemies: MutableList<Enemy>): Enemy? {
        var nearest: Double = Double.MAX_VALUE
        var distance: Double = 0.0
        var en: Enemy = enemies[0]

        for (e in enemies) {
            val aDist: Float = Math.abs(x - e.x).toFloat()
            val bDist: Float = Math.abs(y - e.y).toFloat()
            var auxPit: Float = ((aDist * aDist) + (bDist * bDist));
            distance = Math.sqrt(auxPit.toDouble());

            if (distance < nearest) {
                en = e
                nearest = distance
            }
        }
        if (nearest > RANGE)
            return null
        return en
    }

    /**
     * Taking the nearest enemy, creates a shoot object with the ideal speed and angle
     */
    fun shootNearest(
        enemies: MutableList<Enemy>,
        shoots: MutableList<Shoot>,
        resources: Resources
    ) {
        lastShoot--
        if (enemies.size > 0 && lastShoot <= 0) {
            lastShoot = Random.nextInt(60, 120)
            var dirShot: Double = calculateDirection(enemies)

            if (dirShot > 0) {
                var xss: Double
                var yss: Double

                if (dirShot < 90) {
                    xss = 1.0 * dirShot
                    yss = -1.0 * (90 - dirShot)
                } else if (dirShot < 180) {
                    xss = 1.0 * (180 - dirShot)
                    yss = 1.0 * (dirShot - 90)
                } else if (dirShot < 270) {
                    xss = -1.0 * (dirShot - 180)
                    yss = 1.0 * (270 - dirShot)
                } else {
                    xss = -1.0 * (360 - dirShot)
                    yss = -1.0 * (dirShot - 270)
                }

                shoots.add(
                    Shoot(
                        BitmapFactory.decodeResource(resources, R.drawable.shoot1),
                        x,
                        y,
                        xss,
                        yss
                    )
                )
            }
        }
    }


    /**
     * Calculates the angle of the nearest enemy
     */
    private fun calculateDirection(enemies: MutableList<Enemy>): Double {
        var en: Enemy? = findNearest(enemies)

        if (en != null) {

            val posX = x + image.width / 2
            val posY = y + image.height / 2
            val eposX = en.x + en.image.width / 2.0
            val eposY = en.y + en.image.height / 2.0
            var direction: Double
            //Calculate angle
            direction = Math.toDegrees(
                atan2(
                    (posY - eposY), (posX - eposX)
                )
            )

            //Compensate angle of sprite
            direction -= 90.0;

            //Values from atan2 are from -180 to 180, but we need 0 to 360
            if (direction < 0) {
                direction = 360 - (-direction);
            }
            //Used this formula: https://www.gamefromscratch.com/post/2012/11/18/GameDev-math-recipes-Rotating-to-face-a-point.aspx
            return direction;

        }
        return -1.0
    }

    /**
     * Rotate the canon to the nearest enemy
     */
    fun aimNearest(enemies: MutableList<Enemy>) {

        if (enemies.size > 0) {

            val lastAngle = angleAIM
            //Returns -1 when no enemy is near, so we put the turret in standard position
            angleAIM = calculateDirection(enemies).toFloat()
            if (angleAIM < 0)
                angleAIM = UP
            //Smooth the rotation
            else {

                if (!(lastAngle > 345 && angleAIM < 15 || (angleAIM > 345 && lastAngle < 15)))
                    if (angleAIM > lastAngle)
                        angleAIM = lastAngle + (angleAIM - lastAngle) * SMOOTHPERC
                    else if (angleAIM < lastAngle)
                        angleAIM = lastAngle - (lastAngle - angleAIM) * SMOOTHPERC

            }
        }
    }
}