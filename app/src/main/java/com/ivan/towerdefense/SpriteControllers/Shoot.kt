package com.ivan.towerdefense.SpriteControllers

import android.graphics.Bitmap



class Shoot(image: Bitmap, x: Int, y: Int, xSpeed:Double, ySpeed:Double) : GenericItem(image, x, y) {

    var active:Boolean = true

    private val xSpeed:Double = xSpeed/3
    private val ySpeed:Double = ySpeed/3

    fun move()
    {
        x = (x+xSpeed).toInt()
        y = (y+ySpeed).toInt()

    }

}