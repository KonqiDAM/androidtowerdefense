package com.ivan.towerdefense.SpriteControllers

import android.graphics.Bitmap

class MenuItem(image: Bitmap, x: Int, y: Int) : GenericItem(image, x, y) {


    fun checkTouch(xTouch: Int, yTouch: Int): Boolean {
        return xTouch > x && xTouch < x+w && yTouch>y && yTouch < y+h
    }

}