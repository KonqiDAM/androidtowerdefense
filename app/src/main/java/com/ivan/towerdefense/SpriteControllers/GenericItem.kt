package com.ivan.towerdefense.SpriteControllers

import android.graphics.*
import com.ivan.towerdefense.DEBUGING

const val RIGHT:Float = 90f
const val LEFT = 270f
const val DOWN = 180f
const val UP = 0f

abstract class GenericItem(val image: Bitmap, x: Int, y: Int) {


    var x: Int = 0
    var y: Int = 0
    val w: Int = image.width
    val h: Int = image.height
    var angleAIM:Float = UP

    init {
        this.x = x;
        this.y = y;
    }

    /**
     * Draws the object on to the canvas.
     */
    open fun draw(canvas: Canvas) {

        //Save actual canvas position
        canvas.save();
        //Rotates all canvas from the center of sprite
        canvas.rotate(angleAIM,x+image.width/2f,y.toFloat()+image.height/2f);
        canvas.drawBitmap(image, x.toFloat(), y.toFloat(), null)
        //Draws and restpres canvas
        canvas.restore();

        if(DEBUGING) {
            val rectangle: Rect = Rect(
                this.x + w / 3,
                this.y + w / 3,
                this.x + this.w - w / 3,
                this.y + this.h - w / 3
            )
            canvas.drawRect(rectangle, Paint())
        }
    }
}