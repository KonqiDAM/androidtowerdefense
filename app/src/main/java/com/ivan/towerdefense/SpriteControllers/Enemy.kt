package com.ivan.towerdefense.SpriteControllers

import android.graphics.Bitmap
import android.graphics.Rect
import com.ivan.towerdefense.utils.Coordinates
import kotlin.math.absoluteValue

class Enemy(image: Bitmap, x: Int, y: Int, pathPoints: Array<Coordinates>, time:Int) :
    GenericItem(image, x, y) {

    var active:Boolean = true
    var isWaiting:Boolean = true
    var timeLefToStart:Int = time

    private val xSpeed = 3
    private val ySpeed = 3
    private val path = pathPoints
    private var actualPoint = 0

    /**
     * Create a rectangle for each object with reduced size, in this case these are magic numbers
     * I mesured que sprites with gimp to know how much I should reduce the width and height of the
     * rectangles to make a mor realistic hitbox
     */
    fun checkCollision(shoot: Shoot): Boolean {

        //if one of them is not active just don't check, because is already dead
        if( !(this.active && shoot.active ))
            return false
        val shotRectanble: Rect = Rect(
            shoot.x + w / 3,
            shoot.y + w / 3,
            shoot.x + shoot.w - w / 3,
            shoot.y + shoot.h - w / 3
        )

        val enemyRectangle: Rect =
            Rect(this.x + w / 3, this.y + w / 3, this.x + this.w - w / 3, this.y + this.h - w / 3)

        if (shotRectanble.intersect(enemyRectangle)) {
            shoot.active = false
            this.active = false
            return true
        }

        return false
    }

    /**
     *
     * Moves from point to pint by small increments
     */
    fun move(tileSize: Int) {
        val auxX = (path[actualPoint].x) * tileSize
        val auxY = (path[actualPoint].y) * tileSize


        if ((auxX - x).absoluteValue < xSpeed)
            x = auxX
        when {
            auxX < x -> {
                x -= xSpeed
                angleAIM = DOWN
            }
            auxX > x -> {
                x += xSpeed
                angleAIM = UP
            }
        }

        if ((auxY - y).absoluteValue < ySpeed)
            y = auxY
        when {
            auxY < y -> {
                y -= ySpeed
                angleAIM = RIGHT
            }
            auxY > y -> {
                y += ySpeed
                angleAIM = LEFT
            }
        }
        if ((auxX - x).absoluteValue <= xSpeed
            && (auxY - y).absoluteValue <= ySpeed
            && actualPoint < path.size - 1
        ) {
            actualPoint++
        }

    }

}