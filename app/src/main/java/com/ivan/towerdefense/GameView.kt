//Game by Ivan Lazcano Sindin
package com.ivan.towerdefense

import android.content.Context
import android.content.res.Resources
import android.graphics.*
import android.util.AttributeSet
import android.view.*
import android.widget.Toast
import com.ivan.towerdefense.SpriteControllers.*
import com.ivan.towerdefense.SpriteControllers.MenuItem
import com.ivan.towerdefense.utils.Coordinates

const val TURRET: Int = 1
const val MISILELAUNCHER: Int = 2
const val PLATFORM: Int = 3
const val BASE: Int = 4
const val SPAWN: Int = 5
const val NONE: Int = 0
const val MINTIMETOUCH: Int = 150
const val DEBUGING: Boolean = false
const val ENEMIESPERROUND: Int = 25
const val TIMEBETWEENEMIES: Int = 15

class GameView(context: Context, attributes: AttributeSet) : SurfaceView(context, attributes),
    SurfaceHolder.Callback {

    val TEXT_SIZE = BitmapFactory.decodeResource(resources, R.drawable.automatic_gun).width * 0.25f
    val SPACING: Float =
        BitmapFactory.decodeResource(resources, R.drawable.automatic_gun).width * 0.05f

    private var p: Paint = Paint()
    private var gameEnded: Boolean = false
    private var isConstructTime: Boolean = true
    private var roundNumber: Int = 1
    private var amountLives: Int = 5
    private val con: Context = context
    private val thread: GameThread
    private var remainingPlatform: Int = 10
    private var map = arrayOf<Array<Int>>()
    private var auxMap = map
    private var points: Array<Coordinates> = arrayOf()
    private var tilesSize = BitmapFactory.decodeResource(resources, R.drawable.automatic_gun).width
    private var touched: Boolean = false
    private var touched_x: Int = 0
    private var touched_y: Int = 0
    private var lastTouch = System.currentTimeMillis()
    private val brushGreen = Paint()
    private val brushYellow = Paint()
    private val brushRed = Paint()
    private var enemies: MutableList<Enemy> = mutableListOf<Enemy>()
    private var groundTiles: MutableList<Ground> = mutableListOf<Ground>()
    private var playerGuns: MutableList<GenericGun> = mutableListOf<GenericGun>()
    private var platforms: MutableList<Ground> = mutableListOf<Ground>()
    private var shoots: MutableList<Shoot> = mutableListOf<Shoot>()
    private val screenWidth = Resources.getSystem().displayMetrics.widthPixels
    private val screenHeight = Resources.getSystem().displayMetrics.heightPixels
    private val amountTilesX = (screenWidth - tilesSize) / tilesSize
    private val amountTilesY = (screenHeight - tilesSize * 2) / tilesSize
    private var turret: MenuItem? = null
    private var misileLauncher: MenuItem? = null
    private var platform: MenuItem? = null
    private var startGame: MenuItem? = null
    private var isConstructing: Boolean = false
    private var typeConstructing: Int = NONE
    private val spawnXpos = (0..amountTilesX).random()
    private val baseXpos = (0..amountTilesX).random()
    private val imageGameOver = BitmapFactory.decodeResource(resources, R.drawable.over)
    private val rectangleGameOver = Rect(
        0,
        0,
        screenWidth,
        screenHeight
    )

    init {
        // add callback
        holder.addCallback(this)

        // instantiate the game thread
        thread = GameThread(holder, this)
    }

    /**
     * Creates map floor with spawn and base
     */
    fun createTiles() {
        for (x in 0..amountTilesX) {
            for (y in 0..amountTilesY) {
                var drawable = R.drawable.ground;
                if (y == 0 && x == spawnXpos) {
                    drawable = R.drawable.spawn;
                } else if (y == amountTilesY && x == baseXpos) {
                    drawable = R.drawable.base;
                }
                groundTiles.add(
                    Ground(
                        BitmapFactory.decodeResource(resources, drawable),
                        x * tilesSize,
                        y * tilesSize
                    )
                )
            }
        }
    }

    /**
     * Puts UI items in place
     */
    fun createUI() {
        turret = MenuItem(
            BitmapFactory.decodeResource(resources, R.drawable.automatic_gun),
            0,
            tilesSize * (amountTilesY + 1)
        )
        misileLauncher = MenuItem(
            BitmapFactory.decodeResource(resources, R.drawable.misile_launcher),
            tilesSize,
            tilesSize * (amountTilesY + 1)
        )
        platform = MenuItem(
            BitmapFactory.decodeResource(resources, R.drawable.ground2),
            tilesSize * 2,
            tilesSize * (amountTilesY + 1)
        )
        startGame = MenuItem(
            BitmapFactory.decodeResource(resources, R.drawable.start),
            tilesSize * 3,
            tilesSize * (amountTilesY + 1)
        )
    }

    /**
     * Creates map of the game for internal purpose like calculate routes and
     * to know if player is allowed to construct
     */
    fun createMap() {

        for (y in 0..amountTilesY) {
            var array = arrayOf<Int>()
            for (x in 0..amountTilesX) {
                array += if (y == amountTilesY && x == baseXpos)
                    BASE
                else if (y == 0 && x == spawnXpos)
                    SPAWN
                else
                    NONE
            }
            map += array
        }
    }

    /**
     * Deep copy on map
     */
    fun Array<Array<Int>>.copy() = Array(size) { get(it).clone() }

    /**
     * prints map for debuging purpose
     */
    fun print2Dmap() {
        for (y in auxMap) {
            for (x in y) {
                print(x)
            }
            println()
        }
    }

    /**
     * Initialises everything in the moment the canvas is ready to be used
     */
    override fun surfaceCreated(surfaceHolder: SurfaceHolder) {
        createTiles()
        createUI()
        createMap()
        brushGreen.setARGB(255, 50, 250, 50)
        brushGreen.textSize = TEXT_SIZE
        brushGreen.typeface = Typeface.SERIF
        brushYellow.setARGB(255, 250, 250, 10)
        brushYellow.textSize = TEXT_SIZE
        brushYellow.typeface = Typeface.SERIF
        brushRed.setARGB(255, 255, 20, 20)
        brushRed.textSize = TEXT_SIZE
        brushRed.typeface = Typeface.SERIF
        // start the game thread
        auxMap = map.copy()
        tracePath(spawnXpos, 0)
        p.color = Color.parseColor("#ff1111")
        p.strokeWidth = 10F
        p.style = Paint.Style.STROKE
        p.isAntiAlias = true
        p.isDither = true
        thread.setRunning(true)
        thread.start()
    }

    override fun surfaceChanged(surfaceHolder: SurfaceHolder, i: Int, i1: Int, i2: Int) {

    }

    override fun surfaceDestroyed(surfaceHolder: SurfaceHolder) {
        var retry = true
        while (retry) {
            try {
                thread.setRunning(false)
                thread.join()
            } catch (e: Exception) {
                e.printStackTrace()
            }

            retry = false
        }
    }

    /**
     * Game logic live movment of units and playerGuns shoots
     */
    fun update() {
        if (!gameEnded) {
            if (amountLives <= 0) {
                enemies.clear()
                shoots.clear()
                playerGuns.clear()
                platforms.clear()
                isConstructTime = false
                remainingPlatform = 0
                gameEnded = true
            }

            for (e in enemies) {
                //Only move eney if it's his time to start
                if (e.isWaiting) {
                    e.timeLefToStart -= 1;
                    if (e.timeLefToStart <= 0)
                        e.isWaiting = false;
                } else
                    e.move(tilesSize);
            }
            val auxCount = enemies.size
            //Delete enemies that reach base
            enemies =
                enemies.filterNot { e -> e.x == baseXpos * tilesSize && e.y == amountTilesY * tilesSize }
                    .toMutableList()
            //take 1 live per enemy that reached base
            amountLives -= -enemies.size + auxCount
            for (t in playerGuns) {
                t.aimNearest(enemies)
                t.shootNearest(enemies, shoots, resources)
            }
            for (s in shoots) {
                s.move()
            }
            for (e in enemies) {
                for (s in shoots) {
                    e.checkCollision(s)
                }
            }
            shoots = shoots.filter { s -> s.active }.toMutableList()
            enemies = enemies.filter { e -> e.active }.toMutableList()

            //Give user some resources and allow to construct when round ends
            if (!isConstructTime && enemies.size == 0 && !gameEnded) {
                isConstructTime = true
                remainingPlatform += 6
                roundNumber++
            }
        }
    }

    /**
     * Draws the elements that the player constructed
     */
    fun drawUserUnits(canvas: Canvas) {
        for (f in platforms) {
            f.draw(canvas)
        }
        for (t in playerGuns) {
            t.draw(canvas)
        }

    }

    /**
     * Draws the user info on screen
     */
    fun drawText(canvas: Canvas) {
        var auxBrush = brushGreen
        if (remainingPlatform < 2)
            auxBrush = brushRed
        else if (remainingPlatform < 5)
            auxBrush = brushYellow
        canvas.drawText(
            "Platforms: $remainingPlatform",
            tilesSize * 4f,
            amountTilesY * tilesSize * 1f + tilesSize + TEXT_SIZE * 2 + SPACING * 2,
            auxBrush
        )

        auxBrush = brushGreen
        canvas.drawText(
            "Round: $roundNumber",
            tilesSize * 4f,
            amountTilesY * tilesSize * 1f + tilesSize + TEXT_SIZE * 1 + SPACING,
            auxBrush
        )
        if (amountLives < 2)
            auxBrush = brushRed
        else if (amountLives < 4)
            auxBrush = brushYellow
        else
            auxBrush = brushGreen

        canvas.drawText(
            "Health: $amountLives",
            tilesSize * 4f,
            amountTilesY * tilesSize * 1f + tilesSize + TEXT_SIZE * 3 + SPACING * 3,
            auxBrush
        )
    }

    /**
     * Called when must draw on canvas
     */
    override fun draw(canvas: Canvas) {
        super.draw(canvas)

        if (gameEnded) {
            //Draws BIG image filling all the screen
            canvas.drawBitmap(imageGameOver, null, rectangleGameOver, null)

        } else {
            for (groundTile in groundTiles) {
                groundTile.draw(canvas)
            }
            drawUserUnits(canvas)
            var count = enemies.size
            while (count-- > 0) {
                try {
                    if (!enemies[count].isWaiting)
                        enemies[count].draw(canvas)
                } catch (e: Exception) {
                }
            }

            for (s in shoots) {
                s.draw(canvas)
            }

            drawText(canvas)



            turret!!.draw(canvas)
            misileLauncher!!.draw(canvas)
            platform!!.draw(canvas)
            startGame!!.draw(canvas)
            drawRectangleOverConstructionItem(canvas)
        }
    }

    /**
     *  Draw a rectangle over the selected construction item
     */
    fun drawRectangleOverConstructionItem(canvas:Canvas)
    {
        var x: Float = 0f
        var y: Float = 0f
        if (isConstructing) {
            when (typeConstructing) {
                TURRET -> {
                    x = turret!!.x.toFloat(); y = turret!!.y.toFloat();
                }
                PLATFORM -> {
                    x = platform!!.x.toFloat(); y = platform!!.y.toFloat();
                }
                MISILELAUNCHER -> {
                    x = misileLauncher!!.x.toFloat(); y = misileLauncher!!.y.toFloat();
                }

            }

            canvas.drawRect(
                x,
                y,
                x + tilesSize.toFloat(),
                y + tilesSize.toFloat(),
                p
            )
        }
    }

    /**
     * Checks if the selected place is empty or has a platform
     */
    fun canConstruct(typeUnder: Int): Boolean {
        points = arrayOf()


        if (!tracePath(spawnXpos, 0))
            Toast.makeText(
                con,
                "Must leave a path for enemies!!!",
                Toast.LENGTH_SHORT
            ).show()
        else if (typeUnder == NONE && typeConstructing != PLATFORM)
            Toast.makeText(
                con,
                "Guns go over platforms, put one first",
                Toast.LENGTH_SHORT
            ).show()
        else if (typeUnder == NONE && remainingPlatform <= 0)
            Toast.makeText(
                con,
                "You don't have platforms left!!",
                Toast.LENGTH_SHORT
            ).show()
        else if (typeUnder != NONE && typeUnder != PLATFORM && typeConstructing != PLATFORM)
            Toast.makeText(con, "Spot alredy used!", Toast.LENGTH_SHORT).show()
        else if (typeUnder != NONE && typeConstructing == PLATFORM)
            Toast.makeText(
                con,
                "Platform only can be constructed on blank spot",
                Toast.LENGTH_SHORT
            ).show()
        else
            return true
        return false
    }

    /**
     * Player tries to put a new item in the map
     */
    fun constructionMenu() {
        if (isConstructing) {
            val tileX = touched_x / tilesSize
            val tileY = touched_y / tilesSize
            val auxX = tileX * tilesSize
            val auxY = tileY * tilesSize
            //Only allow to construct in the created tiles
            if (auxX <= tilesSize * amountTilesX && auxY <= tilesSize * amountTilesY) {
                //isConstructing = false
                auxMap = map.copy()
                auxMap[tileY][tileX] = 9
                if (canConstruct(map[tileY][tileX])) {
                    when (typeConstructing) {
                        TURRET -> {
                            map[tileY][tileX] = TURRET
                            playerGuns.add(
                                Turret(
                                    BitmapFactory.decodeResource(
                                        resources,
                                        R.drawable.automatic_gun
                                    ), auxX, auxY
                                )
                            )
                        }
                        MISILELAUNCHER -> {
                            map[tileY][tileX] = MISILELAUNCHER
                            playerGuns.add(
                                MissileLauncher(
                                    BitmapFactory.decodeResource(
                                        resources,
                                        R.drawable.misile_launcher
                                    ), auxX, auxY
                                )
                            )
                        }
                        PLATFORM -> {
                            remainingPlatform--
                            map[tileY][tileX] = PLATFORM
                            platforms.add(
                                Ground(
                                    BitmapFactory.decodeResource(
                                        resources,
                                        R.drawable.ground2
                                    ), auxX, auxY
                                )
                            )
                        }
                    }
                    //print2Dmap()
                }
            }
        }
    }

    /**
     * Checks if player wants to construct something
     */
    fun checkUIClick() {
        if (isConstructTime) {
            if (turret!!.checkTouch(touched_x, touched_y)) {
                isConstructing = true
                typeConstructing = TURRET
            }
            if (misileLauncher!!.checkTouch(touched_x, touched_y)) {
                isConstructing = true
                typeConstructing = MISILELAUNCHER
            }
            if (platform!!.checkTouch(touched_x, touched_y)) {
                isConstructing = true
                typeConstructing = PLATFORM
            }
            if (startGame!!.checkTouch(touched_x, touched_y)) {
                isConstructing = false
                createWaveOfEnemies()
            }
        }
    }

    /**
     * Crates the wave taking in account the round number
     */
    fun createWaveOfEnemies() {
        createEnemy(0)
        for (i in 0..(roundNumber * ENEMIESPERROUND)) {
            createEnemy(TIMEBETWEENEMIES * i)
        }
        isConstructTime = false
    }

    /**
     * Creates a enemy on spawn with a delay timer
     */
    fun createEnemy(timer: Int) {
        var sprite: Int = R.drawable.sold2
        if (timer % (TIMEBETWEENEMIES * 2) == 0)
            sprite = R.drawable.sold1
        enemies.add(
            Enemy(
                BitmapFactory.decodeResource(resources, sprite),
                spawnXpos * tilesSize,
                0,
                points.reversedArray(),
                timer
            )
        )
    }

    /**
     * Player touches screen
     */
    override fun onTouchEvent(event: MotionEvent): Boolean {

        if (!gameEnded) {
            //Delay to avoid multiple touches
            if (System.currentTimeMillis() - lastTouch < MINTIMETOUCH)
                return true

            lastTouch = System.currentTimeMillis()
            touched_x = event.x.toInt()
            touched_y = event.y.toInt()

            val action = event.action
            when (action) {
                MotionEvent.ACTION_DOWN -> touched = true
                MotionEvent.ACTION_MOVE -> touched = true
                MotionEvent.ACTION_UP -> touched = false
                MotionEvent.ACTION_CANCEL -> touched = false
                MotionEvent.ACTION_OUTSIDE -> touched = false
            }

            constructionMenu()
            checkUIClick()
        }
        return true
    }

    /**
     * Solves map and creates a path
     */
    private fun tracePath(actualX: Int, actualY: Int): Boolean {

        if (actualX < 0 || actualY < 0 || actualX > amountTilesX || actualY > amountTilesY) {
            return false
        }

        if (auxMap[actualY][actualX] == BASE) {
            return true
        }

        if (auxMap[actualY][actualX] != NONE && auxMap[actualY][actualX] != SPAWN) {
            auxMap[actualY][actualX] = 9
            return false
        }

        auxMap[actualY][actualX] = 9

        if (tracePath(actualX, actualY + 1)) {
            points += Coordinates(actualX, actualY + 1)
            return true
        }

        if (tracePath(actualX - 1, actualY)) {
            points += Coordinates(actualX - 1, actualY)
            return true
        }

        if (tracePath(actualX + 1, actualY)) {
            points += Coordinates(actualX + 1, actualY)
            return true
        }

        if (tracePath(actualX, actualY - 1)) {
            points += Coordinates(actualX, actualY - 1)
            return true
        }
        return false
    }
}
