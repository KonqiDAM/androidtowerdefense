# Tower defense game 
## Features so far
* Can start a new game
* Game adapts to players phone and generates the exact amount of needed tiles
* Player can construct turrets
* Only can construct on the map and not outside it
* Smart hiding of the navigation bar and status bar
* Player must construct a platform
* 2 types of turrets
* Enemy spawn tile and base tile
* there must be a posible path from spawn-base
* Platforms limit
* Generates a path from spawn to base
* Spawn of enemies and they try to go to base(bottom)
* Turrets always face the nearest enemy
* Rotations has acceleration to be smooth
* Enemies walk from spawn to base
* Enemies do damge to base and disapear
* Text changes color on demand depending situation
* Turret shoot enemies
* Turrets range
* Turrets shoots every 1 to 2 seconds
* Hitbox for enemy and shoots, reduced hitbox to real sprite and not transparent part
* Enemies die and shoot disapears
* Enemis spawn in a spaced time
* UI is blocked when round starts
* UI shows round number, remaining platforms and health
* When player looses, a "game over" screen appears
* Player can loose
* Rounds, every round player has more resources and appear more enemies
* Icon changed
* Added a tutorial screen
* Game is locked in portrait

## Planned:

* ~~Money and turrets cost money~~
* ~~Enemies drop money~~
* Can select dificulty
* ~~Can select amount of money to start~~
* Implement a faster and beter algorithm to find a path from spawn-base
* Implement the sorthest path from spawn-base
* ??????????
